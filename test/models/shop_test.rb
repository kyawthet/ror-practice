require 'test_helper'

class ShopTest < ActiveSupport::TestCase
  test "should not save shop without shop_name" do
  	shop = Shop.new
  	assert_not shop.save, "Saved the article without a title"
  end
end