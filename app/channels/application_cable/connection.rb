module ApplicationCable
  class Connection < ActionCable::Connection::Base
  	identified_by :current_user

  	def connect
  		self.current_user = find_user_by_id
  	end

  	private
  	def find_user_by_id
  	  if verified_user = User.find_by(id: cookies.signed[:current_user_id])
        verified_user
      else
        reject_unauthorized_connection
      end	
  	end

  end
end