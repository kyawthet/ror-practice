module UserHelper
	def is_current_user(user_id)
		user_id === session[:current_user_id].to_i if session[:current_user_id]
	end
end
