class EmailValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless value =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
      record.errors[attribute] << (options[:message] || "Invalid E-mail")
    end
  end
end

class User < ApplicationRecord
	has_many :shop, inverse_of: :user, dependent: :destroy
  has_many :product, through: :shop

  has_secure_password :validations => false
	validates :name, presence: true
	validates :email, presence: true, email: {message: 'Invalid E-mail'}, uniqueness: true #on: :create
	validates :password, presence: true,confirmation: true,length: {in: 6..20}
  validates :password_confirmation, presence: true
	enum provider: { self: 0, facebook: 1, google: 2, linkedin: 3 }
end