class Product < ApplicationRecord
	belongs_to :shop, inverse_of: :product #, optional: true
	validates :product_name, presence: true
	validates :product_desc, presence: true
	# validates :product_image, presence: true
	mount_uploader :product_image, AvatarUploader

	def extension_whitelist
    	%w(jpg jpeg gif png)
  	end
end
