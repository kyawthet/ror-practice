class Shop < ApplicationRecord
	belongs_to :user, inverse_of: :shop #, optional: true
	has_many :product, inverse_of: :shop
    
	validates :shop_name, presence: true, length: { minimum: 3 }
	validates :shop_addr, presence: true, length: { minimum: 5 } #allow_blank
	validates :shop_desc, presence: true, length: { minimum: 5 } #allow_blank
end
