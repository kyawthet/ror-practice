class SignupMailer < ApplicationMailer
	def thanks_singup(user)
		@user = user
		@url = 'http://www.shop.com'
		mail(to: @user[:email], subject: "Welcome To My Site")
	end
end