class ApplicationMailer < ActionMailer::Base
  default from: 'kyawthet.2017.flc@gmail.com'
  layout 'mailer'
end
