class ShopsController < ApplicationController
  before_action :if_unauthorize, except: [:index,:show]
    #https://www.youtube.com/watch?v=KADfCLGQWmI #register
  def index
    # @shops = Shop.all
    # HourlyEmailJob.perform_later()
    # SignupMailer.thanks_singup(@user).deliver_later
    @shops = Shop.paginate(:page => params[:page], :per_page => 5 )
  end

  def new
    WebNotificationsChannel.broadcast_to(
            current_user,
            title: 'New things!',
            body: 'All the news fit to print'
          )
    @shop = Shop.new
  end

  def show
    begin
      @shop = Shop.includes(:product).order("products.id DESC").find(params[:id])
    rescue ActiveRecord::RecordNotFound
      flash[:error] = 'No Shop Found.'
      redirect_to shops_url
    end
  end

  def create
    @user = User.find(session[:current_user_id])
    @shop = @user.shop.create(shop_parameters)
    if @shop.save
      redirect_to @shop
    else
      render 'new'
    end
  end

  def edit
    @shop = User.find(session[:current_user_id]).shop
    if @shop.nil?
      flash[:alert] = 'You don\'t have a shop'
      redirect_to shops_url
    elsif @shop.ids.include?(params[:id].to_i)
        @shop = Shop.find(params[:id].to_i)
    else
      flash[:alert] = 'It is not your shop'
      redirect_to shops_url
    end    
  end

  def update
    @shop = Shop.find(params[:id])
    if @shop.update(shop_parameters)
      flash[:notice] = 'Successfully updated your shop'
      redirect_to @shop
    else
      render 'edit'
    end
  end

  def destory
    @shop = Shop.find(params[:id])
    @shop.destory
    redirect_to @shops_path
  end

  private
  def shop_parameters
    params.require(:shop).permit(:shop_name,:shop_addr,:shop_desc)
  end

end