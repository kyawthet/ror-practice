class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  # protect_from_forgery prepend: true
  helper_method :current_user, :logged_in
# @current_user ||= User.find(session[:current_user_id])
  def current_user
  	true if session[:current_user_id]
  end

  def logged_in
  	current_user != nil
  end

  def authorize
  	redirect_to root_url if current_user
  end

  def if_unauthorize
  	redirect_to '/user/signin' unless current_user
  end

end