class UsersController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :authorize, only: [:signin, :signup], except: [:signout]
  before_action :if_unauthorize, only: [:index, :signout], except: [:signin, :signup]
  def shop_list
    @user = User.find(session[:current_user_id])
    # render plain: @user.shop.inspect
  end

  def signup
  	@user = User.new
  end

  def create
    @user = User.new(register_parameters)
    if @user.save
      # SignupMailer.with(user: @user).thanks_singup.deliver_now
      SignupMailer.thanks_singup(@user).deliver_later
      redirect_to signin_url, notice: 'You are successfully signed up.'
    else
      render 'signup'
    end
  end

  def signin
    @user = User.new
  end

  def login
    @user = User.find_by(email: params[:user][:email])
    if @user.nil?
      flash.now[:login_error] = 'No User Found For This E-mail.'
      render 'signin'
    elsif @user && @user.authenticate(params[:user][:password])
      session[:current_user_id] = @user.id
      cookies.permanent.signed[:current_user_id] = @user.id
      redirect_to root_url, notice: 'You are successfully logged in.'
    else
      flash.now[:login_error] = 'No Password Matched.'
      render 'signin'
    end
  end

  def signout
      session[:current_user_id] = nil
      cookies.delete(:current_user_id)
      redirect_to root_url    
  end

  private
  def register_parameters
    params[:user].merge!({activation_key: ramdom_string, email: params[:user][:email].downcase })
    params.require(:user).permit(:name, :email, :password, :password_confirmation, :activation_key)
  end

  def ramdom_string
    (0...16).map { ('A'..'Z').to_a[rand(5)] << ('0'..'9').to_a[rand(5)] << ('a'..'z').to_a[rand(5)] }.join
  end

end