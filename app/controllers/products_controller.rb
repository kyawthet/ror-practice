class ProductsController < ApplicationController
  before_action :if_unauthorize, except: [:index,:show]
  def index
    @products = Product.includes(:shop).paginate(:page => params[:page], :per_page => 5)
    # render plain: @products.inspect
  end

  def new
  	@shop = Shop.find( params[:shop_id] )
    @product = Product.new
  end

  def create
  	# render plain: product_parameters.inspect
  	@shop_list = User.find(session[:current_user_id]).shop   
    # render plain: {ss: @shop_list.include?(params[:shop_id].to_s)}.inspect
    if @shop_list.ids.include?(params[:shop_id].to_i)
      @shop = Shop.find( params[:shop_id] )
      if @shop.nil?
        flash[:notice] = "Please Create Shop."
        redirect_to  new_shop_url
      else
        @product = @shop.product.create( product_parameters )   
        if @product.save
          flash[:notice] = "It\'s saved."
          redirect_to @shop
        else
          flash.now[:notice] = "It's not saved."
          render :new
        end
      end
    else
      flash[:alert] = 'You could put new products only into your shop.'
      redirect_to shops_path
    end
  end

  def destroy
  	@shop = Shop.find(params[:shop_id])
  	@product = @shop.product
    # render plain: {ss: @product}.inspect
    if @product.empty?
      flash[:notice] = 'You don\'t have a product'
      redirect_to products_url
    elsif @product.ids.include?(params[:id].to_i)
    	Product.find(params[:id]).destroy
      	flash[:notice] = 'Product is deleted.'
      	redirect_to @shop
    else
      flash[:alert] = 'It is not your product'
      redirect_to products_url
    end 
  end

  def edit
  	# @product = User.find(session[:current_user_id]).product
    begin
      @shop = Shop.find( params[:shop_id] )
      @product = Product.find( params[:id] )
      if @product.blank?
        flash[:notice] = 'You don\'t have a product'
        redirect_to products_url
      elsif @shop.product.ids.include?(params[:id].to_i)
      	@product = Product.find(params[:id].to_i)
      else
        flash[:alert] = 'It is not your product'
        redirect_to products_url
      end
    rescue ActiveRecord::RecordNotFound
      flash[:error] = 'No Shop/Product Found To Edit.'
      redirect_to products_index_path
    end 
  end

  def update
    @shop = Shop.find( params[:shop_id] )
    @product = Product.find(params[:id])
    if @product.update(product_parameters)
      flash[:notice] = 'Successfully updated your product'
      redirect_to shop_path(params[:shop_id])
    else
      render 'edit'
    end
  end

  def show
    begin
      @shop = Shop.find(params[:shop_id])
      @product = Product.find(params[:id])
    rescue
      flash[:error] = 'No Shop/Product Found To Display.'
      redirect_to products_index_path
    end
  end

  private
  def product_parameters
  	params.require(:product).permit(:product_name, :product_desc, :product_image, :product_image_cache)
  end

end