class ChangeColumntypeInShop < ActiveRecord::Migration[5.1]
  def change
    change_column  :shops, :shop_desc, :text
  end
end