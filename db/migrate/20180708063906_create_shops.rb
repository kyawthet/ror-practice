class CreateShops < ActiveRecord::Migration[5.1]
  def change
    create_table :shops do |t|
      t.string :shop_name
      t.string :shop_code
      t.string :shop_addr
      t.text :shop_desc
      t.timestamps
    end
  end

  def down
  	drop_table :shops
  end
end
