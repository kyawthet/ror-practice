class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :product_name
      t.string :product_code
      t.text :product_desc
      t.string :product_image
      t.references :shop, foreign_key: true
      t.timestamps
    end
  end
end