class ChangeColumntypeInProducts < ActiveRecord::Migration[5.1]
  def change
    change_column  :products, :product_desc, :text
  end
end
