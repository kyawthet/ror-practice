class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :password
      t.string :activation_key
      t.column :provider, :integer, default: 0
      t.timestamps
    end
  end
end