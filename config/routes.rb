Rails.application.routes.draw do

  require 'resque/server'
  mount Resque::Server, at: '/jobs'
  mount ActionCable.server => '/cable'
  get 'products/index'

  get 'user/shop_list', to: 'users#shop_list', as: 'shop_list'
  get 'user/signup', to: 'users#signup', as: 'signup'
  post 'user/create', to: 'users#create'
  get 'user/signin', to: 'users#signin', as: 'signin'
  post 'user/login', to: 'users#login'
  get 'user/signout', to: 'users#signout', as: 'signout'

  resources :shops do
    resources :products
  end
  root 'shops#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
